'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _socket2 = require('socket.io-client');

var _socket3 = _interopRequireDefault(_socket2);

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _lodash = require('lodash.get');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// Deduced from events triggered by socket.io client
var CLIENT_CONNECTION_STATUS_MAP = {
    CONNECTING: 'connecting',
    CONNECTED: 'connected',
    DISCONNECTED: 'disconnected'
};

var WebSocketClient = function () {
    function WebSocketClient() {
        var _this = this;

        var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

        _classCallCheck(this, WebSocketClient);

        this.socketURL = "ws://localhost:3001/";
        this.tokenAPI = null;
        this.socketTokenKey = null;
        this.socketURLKey = null;
        this.socketOptions = null;
        this.apiTokenKey = null;

        this.token = null;
        this.socket = null;
        this.commandBuffer = [];

        this.instantiated = false;
        this.disconnected = null; // This is `true` only when `disconnect` method is invoked explicitly
        this.connectionStatus = null;
        this.enableDebug = options.enableDebug || false;

        this.getTokenAndConnect = function () {
            _this.socketURL = "ws://localhost:3001/";
            _this.token = 'test';
            // axios.get(this.tokenAPI)
            //     .then((response) => {
            //         if (response.status >= 200 && response.status < 300) {
            //             if (this.disconnected) {
            //                 throw new Error('Socket has been disconnected');
            //             }

            //             this.token = get(response.data, this.apiTokenKey);
            //             if (this.socketURLKey) {
            //                 this.socketURL = get(response.data, this.socketURLKey);
            //             }

            //             if (!this.socketURL) {
            //                 throw new Error('Socket URL value is missing');
            //             }

            //             if (this.socket == null) {
            //                 this.connectToSocket();
            //             } else {
            //                 this.reconnectToSocket();
            //             }
            //         } else {
            //             throw new Error('Failed to get token');
            //         }
            //     })
            //     .catch((error) => {
            //         if (this.disconnected)
            //             return;

            //         this.log(error);
            //         let retryInterval = 1000; // 1 second
            //         if (error && error.response && error.response.status === 401) {
            //             retryInterval = 10 * 1000; // 10 seconds
            //         }
            //         setTimeout(this.getTokenAndConnect, retryInterval);
            //     });
        };

        this.connectToSocket = function () {
            _this.socket = _socket3.default.connect(_this.socketURL, _this.getSocketOptions());
            _this.bindEventListeners();
            _this.executeBufferedCommands();
        };

        this.reconnectToSocket = function () {
            _this.socket.disconnect();
            _this.socket.io.opts.query = _this.socketTokenKey + '=' + _this.token;
            _this.socket.connect();
        };

        this.getSocketOptions = function () {
            return Object.assign({}, _this.socketOptions || {}, {
                query: _this.socketTokenKey + '=' + _this.token
            });
        };

        this.executeBufferedCommands = function () {
            while (_this.commandBuffer.length) {
                var command = _this.commandBuffer.shift();
                _this[command.method].apply(_this, _toConsumableArray(command.arguments));
            }
        };

        this.bindEventListeners = function () {
            _this.socket.on('connect', function () {
                _this.log('connect');
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.CONNECTED;
            });

            _this.socket.on('connect_error', function (error) {
                _this.log('connect_error', error);
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.DISCONNECTED;
            });

            _this.socket.on('reconnect', function () {
                _this.log('reconnect');
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.CONNECTED;
            });

            _this.socket.on('reconnecting', function (number) {
                _this.log('reconnecting', number);
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.CONNECTING;
            });

            _this.socket.on('reconnect_attempt', function (number) {
                _this.log('reconnect_attempt', number);
            });

            _this.socket.on('reconnect_error', function (data) {
                _this.log('reconnect_error', data);
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.DISCONNECTED;
            });

            _this.socket.on('reconnect_failed', function () {
                _this.log('reconnect_failed');
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.DISCONNECTED;
            });

            _this.socket.on('disconnect', function () {
                _this.log('disconnect');
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.DISCONNECTED;
            });

            _this.socket.on('error', function (error) {
                _this.log('error', error);
                _this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.DISCONNECTED;
                if (error.type == 'UnauthorizedError') {
                    _this.getTokenAndConnect();
                } else {
                    _this.reconnectToSocket();
                }
            });
        };

        this.log = function () {
            var _console;

            if (_this.enableDebug) (_console = console).log.apply(_console, arguments);
        };
    }

    _createClass(WebSocketClient, [{
        key: 'connect',
        value: function connect(options) {
            var tokenAPI = options.tokenAPI,
                _options$socketURL = options.socketURL,
                socketURL = _options$socketURL === undefined ? null : _options$socketURL,
                _options$socketURLKey = options.socketURLKey,
                socketURLKey = _options$socketURLKey === undefined ? null : _options$socketURLKey,
                _options$socketTokenK = options.socketTokenKey,
                socketTokenKey = _options$socketTokenK === undefined ? 'token' : _options$socketTokenK,
                _options$socketOption = options.socketOptions,
                socketOptions = _options$socketOption === undefined ? {} : _options$socketOption,
                _options$apiTokenKey = options.apiTokenKey,
                apiTokenKey = _options$apiTokenKey === undefined ? 'token' : _options$apiTokenKey;


            if (!this.instantiated) {
                this.instantiated = true;
                this.tokenAPI = tokenAPI;
                this.socketURL = socketURL;
                this.socketURLKey = socketURLKey;
                this.socketTokenKey = socketTokenKey;
                this.socketOptions = socketOptions;
                this.apiTokenKey = apiTokenKey;
                this.connectionStatus = CLIENT_CONNECTION_STATUS_MAP.CONNECTING;
                this.getTokenAndConnect();
            }

            if (this.disconnected) this.getTokenAndConnect();

            return this;
        }
    }, {
        key: 'disconnect',
        value: function disconnect() {
            this.disconnected = true;
            if (this.socket) {
                this.socket.disconnect();
            }

            return this;
        }
    }, {
        key: 'getConnectionStatus',
        value: function getConnectionStatus() {
            return this.connectionStatus || CLIENT_CONNECTION_STATUS_MAP.DISCONNECTED;
        }
    }, {
        key: 'getSocketInstance',
        value: function getSocketInstance() {
            return this.socket || null;
        }
    }]);

    return WebSocketClient;
}();

/**
 * Public api methods
 * These would get stored in `commandBuffer` if there is no socket instance
 * and would be executed whenever socket gets instantiated.
 */


['send', 'emit', 'on', 'off', 'once'].forEach(function (method) {
    WebSocketClient.prototype[method] = function () {
        if (this.socket) {
            var _socket;

            (_socket = this.socket)[method].apply(_socket, arguments);
        } else {
            this.commandBuffer.push({ method: method });
        }

        return this;
    };
});

exports.default = WebSocketClient;