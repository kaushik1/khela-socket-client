'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = getInstance;

var _websocketclient = require('./websocketclient');

var _websocketclient2 = _interopRequireDefault(_websocketclient);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var instances = {};

function getInstance() {
    var socketIdentifier = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'default';
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    if (!instances.hasOwnProperty(socketIdentifier)) instances[socketIdentifier] = new _websocketclient2.default(options);

    return instances[socketIdentifier];
}