var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var path = require('path');
var jwt = require('jsonwebtoken');
var socketioJWT = require('socketio-jwt');

const JWT_SECRET_KEY = 'dev123456';

app.get('/', function(req, res){
    res.sendFile('index.html', {'root': './'});
});
app.get('/get-token/', function(req, res){
    var token = jwt.sign({
        user_id: 1234,
        user_type: 'student',
        iat: Math.floor(Date.now() / 1000) - 10,
        match_id: 12345,
    }, JWT_SECRET_KEY, {expiresIn: '15s'});
    res.send({token: token});
});
app.use('/static', express.static(path.join(__dirname,'../build/')));

io.use(socketioJWT.authorize({
    secret: JWT_SECRET_KEY,
    handshake: true
}));

io.on('connection', function(socket){
    console.log('a user connected');
    socket.broadcast.emit('chat:message', 'Guest has joined');
    socket.emit('chat:message', 'Socket connection instance created in the server');

    socket.on('disconnect', function(){
        console.log('a user disconnected');
        socket.broadcast.emit('chat:message', 'Guest has left');
    });

    socket.on('chat:message', function(msg){
        console.log('chat:message: ' + msg);
        socket.emit('chat:message', msg);
        socket.broadcast.emit('chat:message', `Guest: ${msg}`);
    });
});

http.listen(3002, function(){
    console.log('listening on *:3002');
});
