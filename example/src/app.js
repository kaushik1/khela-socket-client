require('es6-promise').polyfill();
import websocketClient from '../../src';

const wsClient = websocketClient('example', {enableDebug: true});

wsClient.emit('chat:message', 'A message sent before connecting to socket');

// Adding event listener before making a socket connection
wsClient.on('chat:message', function(msg){
    appendMessage(msg);
});

document.getElementById('chat_form').addEventListener('submit', function(e){
    e.preventDefault();
    var messageInput = document.getElementById('m');
    var message = messageInput.value;
    if (message.length) {
        wsClient.emit('start-session', message);
    }
    messageInput.value = '';
});

function appendMessage(msg) {
    var msgItem = document.createElement('li');
    msgItem.innerHTML = msg;
    document.getElementById('messages').appendChild(msgItem);
}

function connectEvent1(){
    appendMessage('Got connect event 1 in the listener added before making a connection');
}

function connectEvent2(){
    appendMessage('Got connect event 2 in the listener added after making a connection');
}

wsClient.on('connect', connectEvent1);

wsClient.connect({
    socketURL: 'ws://localhost:3001/',
    tokenAPI: '/get-token/',
}).emit('chat:message', 'This message was sent after invoking the connect method');

wsClient.on('connect', function(){
    connectEvent2();

    // Unsubscribe connectEvent1 listener
    wsClient.off('connect', connectEvent1);
});
