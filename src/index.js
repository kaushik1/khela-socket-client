import WebSocketClient from './websocketclient';

const instances = {};

export default function getInstance(socketIdentifier = 'default', options = {}) {
    if (!instances.hasOwnProperty(socketIdentifier))
        instances[socketIdentifier] = new WebSocketClient(options);

    return instances[socketIdentifier];
}
